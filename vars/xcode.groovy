//@Library('ios') _
//xcode("7.3")

def call(xcode_version) {






xcode_cli_tools = '''#!/bin/bash

set +e
response=$(xcode-select -p)
set -e

if [[ "$response" == "" ]]; then

# Get and install Xcode CLI tools
OSX_VERS=$(sw_vers -productVersion | awk -F "." '{print $2}')

# on 10.9+, we can leverage SUS to get the latest CLI tools
if [ "$OSX_VERS" -ge 9 ]; then

# create the placeholder file that's checked by CLI updates' .dist code
# in Apple's SUS catalog
touch /tmp/.com.apple.dt.CommandLineTools.installondemand.in-progress
# find the CLI Tools update
PROD=$(softwareupdate -l | grep "\\*.*Command Line" | head -n 1 | awk -F"*" '{print $2}' | sed -e 's/^ *//' | tr -d '\n')
# install it
softwareupdate -i "$PROD"

# on 10.7/10.8, we instead download from public download URLs, which can be found in
# the dvtdownloadableindex:
# https://devimages.apple.com.edgekey.net/downloads/xcode/simulators/index-3905972D-B609-49CE-8D06-51ADC78E07BC.dvtdownloadableindex
else
[ "$OSX_VERS" -eq 7 ] && DMGURL=http://devimages.apple.com.edgekey.net/downloads/xcode/command_line_tools_for_xcode_os_x_lion_april_2013.dmg
[ "$OSX_VERS" -eq 8 ] && DMGURL=http://devimages.apple.com.edgekey.net/downloads/xcode/command_line_tools_for_osx_mountain_lion_april_2014.dmg

TOOLS=clitools.dmg
curl "$DMGURL" -o "$TOOLS"
TMPMOUNT=`/usr/bin/mktemp -d /tmp/clitools.XXXX`
hdiutil attach "$TOOLS" -mountpoint "$TMPMOUNT"
installer -pkg "$(find $TMPMOUNT -name '*.mpkg')" -target /
hdiutil detach "$TMPMOUNT"
rm -rf "$TMPMOUNT"
rm "$TOOLS"
exit
fi

fi
'''

sh xcode_cli_tools





fastlane()

installed_xcodes = sh returnStdout: true, script: '''set +e ; set +x ; xcversion installed'''
version_string=xcode_version+"	("
if ( installed_xcodes.contains(version_string) ) {
	string1 = installed_xcodes.substring(installed_xcodes.lastIndexOf(version_string));
	string2 = string1.substring(0,string1.indexOf(')'));
	string3 = string2.substring(string2.indexOf('(')+1);
	env.DEVELOPER_DIR = string3
} else {
	sh ''' echo "Install required xcode version in your local machine" ; exit'''
	env.DEVELOPER_DIR = "/Applications/Xcode"+xcode_version+".app"
}

sh "xcodebuild -version"

}
