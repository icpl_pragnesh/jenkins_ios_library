//@Library('ios') _
//fastlane("7.3")

def call() {

fastlane_installation = '''#!/bin/bash

set +e
fastlane_location=$(which fastlane)
set -e
if [[ $fastlane_location != "$HOME/.fastlane/bin/fastlane" ]]; then
set +e
mkdir temp_fastlane
set -e
cd temp_fastlane
curl -o "fastlane.zip" "https://kits-crashlytics-com.s3.amazonaws.com/fastlane/standalone/fastlane.zip"
unzip "fastlane.zip"
rm -r -f "fastlane.zip"
set +e
sh "install"
set -e
cd ..
rm -rf temp_fastlane
fi

set +e
xcversion_location=$(which xcversion)
set -e
if [[ $xcversion_location != "$HOME/.gem/ruby/2.0.0/bin/xcversion" ]]; then
gem install xcode-install -N --user-install
fi
'''


env.PATH=PATH+":"+HOME+"/.fastlane/bin"
env.PATH=PATH+":"+HOME+"/.gem/ruby/2.0.0/bin"
sh fastlane_installation

sh "which fastlane"

}
