//@Library('ios') _
//ios_build()

def call(config) {
config = config?:[:]

features = '''- Download Xcode Command Line Tools
- Download Fastlane
- SetUp Xcode to Build
- Create/Update Certificates And ProvisioingProfiles (Using Fastlane match)
- Build (Using Fastlane gym)
'''
println "Features: "+features

parameters = '''- xcode_version: Version of xcode using which you wants to build (DefaultValue: 8.0)
- ssh_slave_credential_id: credential_id to build using slave which is connected as SSH, it will be used to unlock login keychain (Optional, if you are not using ssh slave)
- match_repo_url: git repository url to store or sync Certificates And ProvisioingProfiles (Optional, if you do not wants to use match)
- apple_credential_id: which is used to Create/Update Certifiates And ProvisioingProfiles (Optional, check documentation of match)
- team_id: which is used to Create/Update Certifiates And ProvisioingProfiles in your account (Optional, check documentation of match)
- export_method: (For match: DefaultValue: development)
- app_identifier: (Optional, if you do not wants to use match)
- project_path: XcodeProjectPath (Optional)
- info_plist_path: plist path(Optional, Required Only if you specify app_identifier externally)
'''
println "Parameters: "+parameters



xcode(config.xcode_version?:"8.0")



def resource = libraryResource 'org/is/ios/Fastfile'
writeFile file: 'fastlane/Fastfile', text: resource

if (config.ssh_slave_credential_id) {
	withCredentials([usernamePassword(credentialsId: config.ssh_slave_credential_id, passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
		env.slave_password = PASSWORD
	}
}

if (config.apple_credential_id) {
	withCredentials([usernamePassword(credentialsId: config.apple_credential_id, passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
			env.APPLE_ID = USERNAME
			env.DELIVER_PASSWORD = PASSWORD
	}
}

env.match_repo_url = config.match_repo_url?:""
env.export_method = config.export_method?:""
env.app_identifier = config.app_identifier?:""
env.team_id = config.team_id?:""
sh '''
set +x
date +\"%Y%m%d%H%M%S\" > timestamp
'''
env.output_directory = WORKSPACE+"/ios_build/"+BUILD_NUMBER+"/"+readFile('timestamp').trim()
env.project_path = config.project_path?:""
env.info_plist_path = config.info_plist_path?:""

sh("fastlane ios_build")
ipa_output_path = sh returnStdout: true, script: '''set +x ; ls ${output_directory}/*.ipa'''

env.APPLE_ID = ""
env.DELIVER_PASSWORD = ""
env.slave_password = ""

env.match_repo_url = ""
env.export_method = ""
env.app_identifier = ""
env.team_id = ""
env.output_directory = ""
env.project_path = ""
env.info_plist_path = ""


dir('fastlane') {
    deleteDir()
}


return ipa_output_path

}
